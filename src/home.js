import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Alert,
} from "react-native";
import sheets from "./axios/axios"; // Importa o objeto que contém as funções Axios
import LButton from "./components/LButton"; // Importa o componente de botão personalizado

const CadastroMesas = () => {
  const [status, setStatus] = useState("1");
  const [capacidade, setCapacidade] = useState("");
  const [reservedTables, setReservedTables] = useState([]);
  const [showReservedTables, setShowReservedTables] = useState(false);
  const [editIndex, setEditIndex] = useState(null);
  const [editStatus, setEditStatus] = useState("1");
  const [editCapacidade, setEditCapacidade] = useState("");

  const reserveTable = async () => {
    try {
      if (status && capacidade) {
        const response = await sheets.cadastroMesa({
          status,
          capacidade,
        });

        if (response.status === 200) {
          Alert.alert("Cadastro realizado com sucesso");
          fetchTables();
        } else {
          Alert.alert("Erro ao cadastrar mesa");
        }
      } else {
        Alert.alert("Preencha todos os campos");
      }
    } catch (error) {
      console.error("Erro ao cadastrar mesa: ", error.response.data);
      Alert.alert(
        "Erro ao cadastrar mesa, por favor, tente novamente: " +
          error.response.data.message
      );
    }
  };

  const fetchTables = async () => {
    try {
      const response = await sheets.getMesas();
      setReservedTables(response.data);
    } catch (error) {
      console.error("Erro ao buscar mesas: ", error.response.data);
      Alert.alert("Erro ao buscar mesas, por favor, tente novamente");
    }
  };

  const editReservation = (mesa) => {
    setEditIndex(mesa.id_mesa);
    setEditStatus(mesa.status.toString());
    setEditCapacidade(mesa.capacidade.toString());
  };

  const saveEdit = async () => {
    try {
      if (editIndex !== null) {
        const response = await sheets.updateMesa(editIndex, {
          status: editStatus,
          capacidade: editCapacidade,
        });

        if (response.status === 200) {
          Alert.alert("Edição realizada com sucesso");
          fetchTables();
          setEditIndex(null);
        } else {
          Alert.alert("Erro ao editar mesa");
        }
      }
    } catch (error) {
      console.error("Erro ao editar mesa: ", error.response.data);
      Alert.alert(
        "Erro ao editar mesa, por favor, tente novamente: " +
          error.response.data.message
      );
    }
  };

  const deleteReservation = async (id_mesa) => {
    try {
      const response = await sheets.deleteMesa(id_mesa);

      if (response.status === 200) {
        Alert.alert("Mesa excluída com sucesso");
        fetchTables();
      } else {
        Alert.alert("Erro ao excluir mesa");
      }
    } catch (error) {
      console.error("Erro ao excluir mesa: ", error.response.data);
      Alert.alert(
        "Erro ao excluir mesa, por favor, tente novamente: " +
          error.response.data.message
      );
    }
  };

  useEffect(() => {
    fetchTables();
  }, []);

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>
        <Text style={styles.titleText}>Cadastro de Mesas</Text>
        <View style={styles.form}>
          <Text style={styles.textoInput}>Status:</Text>
          <View style={styles.checkboxContainer}>
            <TouchableOpacity
              style={[styles.checkbox, status === "1" && styles.checked]}
              onPress={() => setStatus("1")}
            />
            <Text style={styles.checkboxText}>Disponível</Text>
            <TouchableOpacity
              style={[styles.checkbox, status === "0" && styles.checked]}
              onPress={() => setStatus("0")}
            />
            <Text style={styles.checkboxText}>Ocupado</Text>
          </View>
          <Text style={styles.textoInput}>Capacidade:</Text>
          <TextInput
            style={styles.input}
            placeholder="Digite a capacidade"
            keyboardType="numeric"
            value={capacidade}
            onChangeText={(text) => setCapacidade(text)}
          />
          <LButton
            backgroundColor={"#D47608"}
            height={60}
            onPress={reserveTable}
          >
            Cadastrar Mesa
          </LButton>
          <LButton
            backgroundColor={"#751319"}
            height={60}
            onPress={() => setShowReservedTables(!showReservedTables)}
          >
            <Text>
              {showReservedTables ? "Esconder Mesas" : "Mostrar Mesas"}
            </Text>
          </LButton>
        </View>

        {showReservedTables && (
          <View style={styles.reservedTables}>
            <Text style={styles.reservedTablesTitle}>Mesas Cadastradas:</Text>
            <ScrollView>
              {reservedTables.map((reservation) => (
                <View key={reservation.id_mesa} style={styles.reservedTable}>
                  {editIndex === reservation.id_mesa ? (
                    <>
                      <View style={styles.checkboxContainer}>
                        <TouchableOpacity
                          style={[
                            styles.checkbox,
                            editStatus === "1" && styles.checked,
                          ]}
                          onPress={() => setEditStatus("1")}
                        />
                        <Text style={styles.checkboxText}>Disponível</Text>
                        <TouchableOpacity
                          style={[
                            styles.checkbox,
                            editStatus === "0" && styles.checked,
                          ]}
                          onPress={() => setEditStatus("0")}
                        />
                        <Text style={styles.checkboxText}>Ocupado</Text>
                      </View>
                      <TextInput
                        style={styles.input}
                        placeholder="Capacidade"
                        keyboardType="numeric"
                        value={editCapacidade}
                        onChangeText={(text) => setEditCapacidade(text)}
                      />
                      <TouchableOpacity
                        style={styles.saveButton}
                        onPress={saveEdit}
                      >
                        <Text style={styles.buttonText}>Salvar</Text>
                      </TouchableOpacity>
                    </>
                  ) : (
                    <View>
                      <Text>Mesa {reservation.id_mesa}:</Text>
                      {reservation.status == "1" ? (
                        <Text>Status: Disponível</Text>
                      ) : (
                        <Text>Status: Ocupado</Text>
                      )}
                      <Text>Capacidade: {reservation.capacidade}</Text>
                    </View>
                  )}
                  <View style={styles.buttonContainer}>
                    <TouchableOpacity
                      style={styles.editButton}
                      onPress={() => editReservation(reservation)}
                    >
                      <Text style={styles.buttonText}>Editar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.deleteButton}
                      onPress={() => deleteReservation(reservation.id_mesa)}
                    >
                      <Text style={styles.buttonText}>Excluir</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ))}
            </ScrollView>
          </View>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F9F5EB",
  },
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F9F5EB",
    width: "100%",
  },
  titleText: {
    fontSize: 28,
    marginTop: 52,
  },
  form: {
    alignItems: "center",
    marginTop: "13%",
    marginBottom: 44,
  },
  input: {
    backgroundColor: "#F9F5EB",
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: "#000",
    width: 310,
    height: 48,
    marginBottom: 14,
    paddingLeft: 6,
  },
  checkboxContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  checkbox: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "#ccc",
    marginRight: 10,
  },
  checked: {
    backgroundColor: "#D47608",
  },
  checkboxText: {
    fontSize: 16,
  },
  button: {
    backgroundColor: "#751319",
    paddingVertical: 15,
    borderRadius: 5,
    alignItems: "center",
    marginBottom: 10,
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
  },
  reservedTables: {
    marginTop: 20,
    width: "100%",
  },
  reservedTablesTitle: {
    fontSize: 24,
    marginBottom: 10,
    textAlign: "center",
  },
  reservedTable: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
    backgroundColor: "#fff",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  editButton: {
    backgroundColor: "#D47608",
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    alignItems: "center",
  },
  deleteButton: {
    backgroundColor: "#751319",
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    alignItems: "center",
  },
  saveButton: {
    backgroundColor: "#059d0b",
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    alignItems: "center",
    marginTop: 10,
  },
  textoInput: {
    fontSize: 16,
    marginBottom: 5,
  },
});

export default CadastroMesas;
