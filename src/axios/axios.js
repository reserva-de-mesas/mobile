import axios from "axios";

const api = axios.create({
  baseURL: "http://10.89.234.149:5000/restaurante",
  headers: {
    Accept: "application/json",
  },
});

const sheets = {
  login: (cliente) => api.post("/login", cliente),
  cadastro: (cliente) => api.post("/cadastro", cliente),
  getMesas: () => api.get("/mesa"),
  cadastroMesa: (mesa) => api.post("/cadastroMesa", mesa),
  deleteMesa: (id) => api.delete(`/deletarMesa/${id}`),
  updateMesa: (id, mesa) => api.put(`/editarMesa/${id}`, mesa),
  criarReserva: (schedule) => api.post("/criarReserva", schedule),
  getReservas: (id) => api.get(`/reservas/${id}`),
};

export default sheets;
