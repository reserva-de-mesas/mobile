// Importações necessárias do React Native e do arquivo de requisições Axios
import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from "react-native";
import sheets from "./axios/axios"; // Importa o objeto que contém as funções Axios
import LButton from "./components/LButton"; // Importa o componente de botão personalizado
import Icon from "react-native-vector-icons/MaterialCommunityIcons"; // Importa os ícones

// Definição do componente de cadastro
const Cadastro = ({ navigation }) => {
  // Definição dos estados para os campos do formulário
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [nome, setNome] = useState("");
  const [cpf, setCpf] = useState("");
  const [telefone, setTelefone] = useState("");
  const [confirmarSenha, setConfirmarSenha] = useState("");
  const [senhaVisible, setSenhaVisible] = useState(false);
  const [confirmarSenhaVisible, setConfirmarSenhaVisible] = useState(false);

  // Função para lidar com o envio do formulário de cadastro
  const handleCadastroSubmit = async () => {
    try {
      // Verifica se todos os campos estão preenchidos
      if (nome && email && senha && confirmarSenha && cpf && telefone) {
        // Verifica se as senhas coincidem
        if (senha !== confirmarSenha) {
          alert("As senhas informadas não coincidem");
        } else {
          // Envia a requisição de cadastro para a API utilizando Axios
          const response = await sheets.cadastro({
            nome,
            email,
            cpf,
            telefone,
            senha,
            confirmarSenha,
          });
          // Verifica se o cadastro foi realizado com sucesso
          if (response.status === 200) {
            alert("Cadastro realizado com sucesso");
            navigation.navigate("Reserva", { cliente: response.data.cliente });
            // navigation.navigate("Home");
          } else {
            alert("Erro ao cadastrar cliente");
          }
        }
      } else {
        alert("Preencha todos os campos");
      }
    } catch (error) {
      // Trata erros caso ocorra algum problema na requisição
      console.error("Erro ao cadastrar cliente: ", error.response.data);
      alert(error.response.data.error);
    }
  };

  // Estrutura do componente de cadastro
  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>
        <Text style={styles.titleText}>CADASTRAR</Text>
        <View style={styles.form}>
          {/* Campos do formulário */}
          <Text style={styles.textoInput}>Nome</Text>
          <TextInput
            style={styles.input}
            value={nome}
            onChangeText={(text) => setNome(text)}
          />

          <Text style={styles.textoInput}>Telefone</Text>
          <TextInput
            style={styles.input}
            value={telefone}
            keyboardType="numeric"
            maxLength={11}
            onChangeText={(number) => setTelefone(number)}
          />

          <Text style={styles.textoInput}>CPF</Text>
          <TextInput
            style={styles.input}
            value={cpf}
            keyboardType="numeric"
            maxLength={11}
            onChangeText={(text) => setCpf(text.replace(/[^0-9]/g, ""))}
          />

          <Text style={styles.textoInput}>E-mail</Text>
          <TextInput
            style={styles.input}
            value={email}
            onChangeText={(text) => setEmail(text)}
          />

          <Text style={styles.textoInput}>Senha</Text>
          <View style={styles.passwordContainer}>
            <TextInput
              style={styles.inputPassword}
              value={senha}
              onChangeText={(text) => setSenha(text)}
              secureTextEntry={!senhaVisible}
            />
            <TouchableOpacity onPress={() => setSenhaVisible(!senhaVisible)}>
              <Icon
                name={senhaVisible ? "eye-off" : "eye"}
                size={24}
                color="black"
              />
            </TouchableOpacity>
          </View>

          <Text style={styles.textoInput}>Confirmar Senha</Text>
          <View style={styles.passwordContainer}>
            <TextInput
              style={styles.inputPassword}
              value={confirmarSenha}
              onChangeText={(text) => setConfirmarSenha(text)}
              secureTextEntry={!confirmarSenhaVisible}
            />
            <TouchableOpacity
              onPress={() => setConfirmarSenhaVisible(!confirmarSenhaVisible)}
            >
              <Icon
                name={confirmarSenhaVisible ? "eye-off" : "eye"}
                size={24}
                color="black"
              />
            </TouchableOpacity>
          </View>
        </View>
        {/* Botão de cadastro */}
        <LButton
          backgroundColor={"#D47608"}
          height={60}
          onPress={handleCadastroSubmit}
        >
          Cadastrar
        </LButton>
      </View>
    </ScrollView>
  );
};

// Estilos do componente
const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F9F5EB",
  },
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F9F5EB",
    width: "100%",
  },
  titleText: {
    fontSize: 28,
    marginTop: 52,
  },
  form: {
    alignItems: "center",
    marginTop: "13%",
    marginBottom: 44,
  },
  input: {
    backgroundColor: "#F9F5EB",
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: "#000",
    width: 310,
    height: 48,
    marginBottom: 14,
    paddingLeft: 6,
  },
  passwordContainer: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#000",
    borderWidth: 0.5,
    borderRadius: 3,
    backgroundColor: "#F9F5EB",
    width: 310,
    height: 48,
    marginBottom: 14,
  },
  inputPassword: {
    flex: 1,
    paddingLeft: 6,
  },
  textoInput: {
    alignSelf: "flex-start",
    marginLeft: 2,
    fontSize: 16,
  },
});

export default Cadastro; // Exporta o componente Cadastro
