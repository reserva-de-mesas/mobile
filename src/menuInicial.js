import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import { StatusBar } from 'expo-status-bar';

import LButton from "./components/LButton";
 

const MenuInicial = ({navigation})=>{
    
    return(
        <View style={styles.container}>
            <Text style={styles.titleText}>RESERV<Text style={styles.secondText}>EAT</Text></Text>
            <Image style={styles.logo} source={require('./images/logo.png')}/>

            <Text style={styles.subtitulo}>Faça sua reserva já!</Text>
            
            <LButton backgroundColor={'#751319'} marginBottom={40} onPress={()=>{navigation.navigate('Login')}}>Entrar</LButton>
            <LButton backgroundColor={'#D47608'} onPress={() => {navigation.navigate('Cadastro')}}>Cadastrar-se</LButton>

            <StatusBar style="light" />
        </View>
    );

} 

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F9F5EB',
        height: '100%',
        width: '100%',
        
    },
    titleText:{
        fontSize: 30,
        marginTop: 38,
    },
    secondText:{
       color: '#5B1940',
    },
    logo:{
        marginTop: 14,
        width: 270,
        height: 270,
    },
    subtitulo:{
        fontSize: 22,
        marginTop: 30,
        marginBottom: 60,
    }
});
export default MenuInicial;