import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  Button,
  StyleSheet,
  Alert,
  Modal,
  ScrollView, // Adicionei o ScrollView para exibir reservas longas
} from "react-native";
import sheets from "./axios/axios";
import DateTimePicker from "./components/datePicker";
import CheckDays from "./components/checkDays";

const Reserva = ({ route }) => {
  const { cliente } = route.params;
  const [mesas, setMesas] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [showReservationsModal, setShowReservationsModal] = useState(false); // Modal para exibir reservas
  const [schedule, setSchedule] = useState({
    date: null,
    timeStart: null,
    timeEnd: null,
    days: [],
  });
  const [reservas, setReservas] = useState([]); // Estado para armazenar as reservas
  const [selectedMesaId, setSelectedMesaId] = useState(null); // Estado para armazenar o ID da mesa selecionada

  const fetchMesas = async () => {
    try {
      const response = await sheets.getMesas();
      if (!response) {
        throw new Error("Resposta da API é indefinida");
      }
      setMesas(response.data);
    } catch (error) {
      console.error("Erro ao buscar mesas: ", error.message);
      Alert.alert("Erro ao buscar mesas, por favor, tente novamente");
    }
  };

  useEffect(() => {
    fetchMesas();
  }, []);

  const handleReservation = (id_mesa) => {
    console.log("Reservando mesa: ", id_mesa);
    setSelectedMesaId(id_mesa); // Armazena o ID da mesa selecionada
    setShowModal(true);
  };

  const handleConsult = async (id_mesa) => {
    console.log("Consultando mesa: ", id_mesa);
    try {
      const response = await sheets.getReservas(id_mesa);
      if (response.status === 200) {
        setReservas(response.data);
        setShowReservationsModal(true);
      } else {
        Alert.alert("Erro ao consultar reservas");
      }
    } catch (error) {
      console.error("Erro ao consultar reservas: ", error.message);
      Alert.alert("Erro ao consultar reservas, por favor, tente novamente");
    }
  };

  const createSchedule = async () => {
    try {
      const response = await sheets.criarReserva({
        date: schedule.date,
        timeStart: schedule.timeStart,
        timeEnd: schedule.timeEnd,
        cliente: cliente.cpf,
        mesa: selectedMesaId, // Passa o ID da mesa selecionada
      });

      if (response.status === 201) {
        Alert.alert("Reserva realizada com sucesso");
        setShowModal(false);
      } else {
        Alert.alert("Erro ao realizar reserva");
      }
    } catch (error) {
      console.error("Erro ao criar reserva: ", error.response.data);
      Alert.alert("Erro ao criar reserva, por favor, tente novamente");
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{cliente.nome}</Text>
      <FlatList
        data={mesas}
        keyExtractor={(item) => item.id_mesa.toString()}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Text style={styles.itemText}>Mesa: {item.id_mesa}</Text>
            <Text style={styles.itemText}>Capacidade: {item.capacidade}</Text>
            <View style={styles.buttonContainer}>
              <Button
                title="Reservar"
                onPress={() => handleReservation(item.id_mesa)}
                color="blue"
              />
              <Button
                title="Consultar"
                onPress={() => handleConsult(item.id_mesa)}
                color="green"
              />
            </View>
          </View>
        )}
      />

      <Modal visible={showModal} animationType="slide" transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>Formulário de Reserva:</Text>
            <DateTimePicker
              type={"date"}
              buttonTitle={"Data"}
              dateKey={"date"}
              setSchedule={setSchedule}
            />
            <CheckDays selectedDays={schedule.days} setSchedule={setSchedule} />
            <DateTimePicker
              type={"time"}
              buttonTitle={"Hora de Início"}
              dateKey={"timeStart"}
              setSchedule={setSchedule}
            />
            <DateTimePicker
              type={"time"}
              buttonTitle={"Hora de Término"}
              dateKey={"timeEnd"}
              setSchedule={setSchedule}
            />
            <View style={styles.buttonContainer}>
              <Button
                title="Confirmar Reserva"
                onPress={createSchedule}
                color="green"
              />
              <Button
                title="Cancelar"
                onPress={() => setShowModal(false)}
                color="red"
              />
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        visible={showReservationsModal}
        animationType="slide"
        transparent={true}
      >
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>
              Reservas para Mesa {selectedMesaId}:
            </Text>
            <ScrollView style={styles.reservasContainer}>
              {reservas.map((reserva) => (
                <View key={reserva.id_reserva} style={styles.reservaItem}>
                  <Text style={styles.reservaText}>Data: {reserva.date}</Text>
                  <Text style={styles.reservaText}>
                    Início: {reserva.timeStart}
                  </Text>
                  <Text style={styles.reservaText}>
                    Término: {reserva.timeEnd}
                  </Text>
                  <Text style={styles.reservaText}>Cliente: {reserva.cpf}</Text>
                </View>
              ))}
            </ScrollView>
            <View style={styles.buttonContainer}>
              <Button
                title="Fechar"
                onPress={() => setShowReservationsModal(false)}
                color="red"
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F9F5EB",
  },
  title: {
    fontSize: 28,
    marginTop: 52,
  },
  item: {
    backgroundColor: "#fff",
    marginBottom: 20,
    padding: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
  },
  itemText: {
    fontSize: 16,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    backgroundColor: "#084d6e",
    borderRadius: 10,
    padding: 20,
    width: "80%",
    maxHeight: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
  modalTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#fff",
  },
});

export default Reserva;
